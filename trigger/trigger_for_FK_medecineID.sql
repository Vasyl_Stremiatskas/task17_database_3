delimiter //
create trigger _before_insert_medecineID  
before insert on pharmacy_medicine  for each row
begin
 if(select count(*) from medicine where medicine.id = new.medicine_id) = 0 then
 SIGNAL SQLSTATE '45000' SET MYSQL_ERRNO = 30001, MESSAGE_TEXT = " Can't insert record. Foreign medicide_id  key doesn't exist";
 end if;
 end //
 delimiter ;
 
delimiter //
create trigger _before_update_medicineId 
before update on pharmacy_medicine for each row
begin
 if(select count(*) from medicine where medicine.id = new.medicine_id) = 0 then
 SIGNAL SQLSTATE '45000' SET MYSQL_ERRNO = 30001, MESSAGE_TEXT = " Can't update record. Foreign medicine_id key doesn't exist";
 end if;
 end //
 delimiter ;
 
 delimiter //
 create trigger _before_update_medID
 before update on medicine for each row
 begin
 if(old.id <> new.id and (select count(*) from medicine_zone where medicide.id = old.id)<> 0 ) then
 SIGNAL SQLSTATE '45000' SET MYSQL_ERRNO = 30001, MESSAGE_TEXT = 'Can\'t update record. Foreign key updates to medicine_zone table restricted!';
 end if;
 end //
 delimiter ; 
 
 delimiter //
 create trigger _before_delete_medicineID
 before delete on medicine for each row
 begin
 if(select count(*) from medicine where medicine.id = old.id <> 0 ) then
 SIGNAL SQLSTATE '45000' SET MYSQL_ERRNO = 30001, MESSAGE_TEXT = 'Can\'t delete record. FForeign key exists in medicine_zone table!';
 end if;
 end //
 delimiter ;