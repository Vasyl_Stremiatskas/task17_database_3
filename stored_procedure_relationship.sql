 delimiter // 
 use storedpr_db //
 drop procedure if exists create_relationship //
 create procedure create_relationship (name  varchar(30), ministry_code char(10), recipe bit(1),
                                      narcotic bit(1), psychotropic bit(1), zone_id int)
BEGIN
START TRANSACTION;
   INSERT INTO medicine(name, ministry_code, recipe, narcotic,psychotropic ) 
     VALUES(name, ministry_code, recipe, narcotic,psychotropic);

   INSERT INTO medicine_zone (medicine_id, zone_id) 
     VALUES(LAST_INSERT_ID(), zone_id);
COMMIT;
END//
 delimiter ;
 
 
 