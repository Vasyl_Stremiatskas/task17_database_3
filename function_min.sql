CREATE DEFINER=`root`@`localhost` FUNCTION `minExperience`() RETURNS decimal(10,1)
begin

 return ( SELECT MIN(experience) FROM  employee  );
end